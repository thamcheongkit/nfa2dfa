all:
	javac Nfa2dfa.java
	java Nfa2dfa

clean:
	rm *.class
	rm build/Nfa2dfa.jar

jar:
	javac Nfa2dfa.java
	jar cfe build/Nfa2dfa.jar Nfa2dfa *.class
	rm *.class