# to compile
$ javac /path/to/master/directory/Nfa2dfa.java

# to run
$ java /path/to/master/directory/Nfa2dfa

# clean all .class files
$ make clean

# shortcut to compile and run
$ make

# to build .jar executable
javac Nfa2dfa.java
jar cfe build/Nfa2dfa.jar Nfa2dfa *.class