import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;

import java.util.Arrays;
import java.util.*;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.table.DefaultTableModel;

import java.lang.Math;

public class Nfa2dfa {


    public static String[] getAllLists(String[] elements, int lengthOfList) {
        //initialize our returned list with the number of elements calculated above
        String[] allLists = new String[(int)Math.pow(elements.length, lengthOfList)];

        //lists of length 1 are just the original elements
        if(lengthOfList == 1) return elements;
        else {
            //the recursion--get all lists of length 3, length 2, all the way up to 1
            String[] allSublists = getAllLists(elements, lengthOfList - 1);

            //append the sublists to each element
            int arrayIndex = 0;

            for(int i = 0; i < elements.length; i++){
                for(int j = 0; j < allSublists.length; j++){
                    //add the newly appended combination to the list
                    allLists[arrayIndex] = elements[i] + allSublists[j];
                    arrayIndex++;
                }
            }
            return allLists;
        }
    }

    // public static String combine(String target, String input) {
    //     Set<String> set = new HashSet<String>();

    //     // target to array
    //     String[] temp = target.split(",");
    //     for (String i : temp) {
    //         set.add(i);
    //     }

    //     // input to array
    //     String[] temp2 = input.split(",");
    //     for (String i : temp2) {
    //         set.add(i);
    //     }

    //     // set to string
    //     String combined = new String();
    //     for (String i : set) {
    //         combined += ("," + i);
    //     }

    //     combined = combined.substring(1);
    //     // String[] combined = set.toArray(new String[set.size()]);
    //     return combined;
    // }

    public static String combine(String target) {
        Set<String> set = new HashSet<String>();

        // target to array
        String[] temp = target.split(",");
        for (String i : temp) {
            set.add(i);
        }

        // // input to array
        // String[] temp2 = input.split(",");
        // for (String i : temp2) {
        //     set.add(i);
        // }


        if ( set.size()>1 && set.contains("Ø") ) {
            boolean a = set.remove("Ø");
        }

        // set to string
        String combined = new String();
        for (String i : set) {
            combined += ("," + i);
        }


        combined = combined.substring(1);
        // String[] combined = set.toArray(new String[set.size()]);
        return combined;
    }

    public static int[] convertIntegers(ArrayList<Integer> integers)
    {
        int[] ret = new int[integers.size()];
        for (int i=0; i < ret.length; i++)
        {
            ret[i] = integers.get(i).intValue();
        }
        return ret;
    }

    public static int[] getIndex(String target, JTable table) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        ArrayList<Integer> index = new ArrayList<Integer>();

        String[] splittedTarget = target.split(",");

        int row = model.getRowCount();
        for (int i=0; i<row; i++) {
            String iden = table.getValueAt(i, 0).toString();
            // System.out.println("iden: " + iden);
            // System.out.println("target: " + target);
            for (String splitted : splittedTarget) {
                if ( splitted.equals(iden) ) {
                    index.add(i);
                }
            }
        }
        int[] index2 = convertIntegers(index);
        return index2;
    }







    // public static String[] getAllLists(String[] elements, int lengthOfList) {
    //     //initialize our returned list with the number of elements calculated above
    //     String[] allLists = new String[(int)Math.pow(elements.length, lengthOfList)];
    //     ArrayList<String> arraylist = new ArrayList<String>();
    //     //lists of length 1 are just the original elements
    //     if(lengthOfList == 1) return elements;
    //     else {
    //         //the recursion--get all lists of length 3, length 2, all the way up to 1
    //         String[] allSublists = getAllLists(elements, lengthOfList - 1);
    //         System.out.print(Arrays.toString(allSublists));
    //         //append the sublists to each element
    //         int arrayIndex = 0;

    //         for(int i = 0; i < elements.length; i++){
    //             // for(int j = 0; j < allSublists.length; j++){
    //             for(int j = i; j < allSublists.length; j++){
    //                 //add the newly appended combination to the list
    //                 // if (elements[i]!=allSublists[j]) {
    //                     // arraylist.add(elements[i] + "," + allSublists[j]);
    //                 // }
    //                 allLists[arrayIndex] = elements[i] + "," + allSublists[j];
    //                 arrayIndex++;
    //             }
    //         }
    //         System.out.print(arraylist.toString());
    //         return allLists;

    //     }
    // }




    // http://stackoverflow.com/questions/21425346/generate-all-unique-combinations-of-items
    public static void allCombinations(int n) {

        BitSet bs = new BitSet();
        while (bs.length() <= n) {
            System.out.println(bs);
            byte[] b = bs.toByteArray();
            System.out.println(Arrays.toString(b));

            //Inc by 1
            int pos = bs.nextClearBit(0);
            bs.flip(0, pos + 1);
        }
    }



  public static void main(String args[]) {
    JFrame frame = new JFrame("NFA to DFA");


    frame.setLayout(new BorderLayout());
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);









    // Object rowData[][] = { { "Row1-Column1", "Row1-Column2", "Row1-Column3" },
    //     { "Row2-Column1", "Row2-Column2", "Row2-Column3" },
    //     { "Row1-Column1", "Row1-Column2", "Row1-Column3" },
    //     { "Row1-Column1", "Row1-Column2", "Row1-Column3" },
    //     { "Row1-Column1", "Row1-Column2", "Row1-Column3" },
    //     { "Row1-Column1", "Row1-Column2", "Row1-Column3" } };

    Object rowData[][] = { { "z0", "z0,z1,z2", "z1,z2", "z2" },
        { "z1", "Ø", "z1,z2", "z2" },
        { "z2", "Ø", "Ø", "z2" }
    };

    // Object columnNames[] = { "δ", "Column Two", "Column Three" };
    Object columnNames[] = { "δ_N", 0, 1, 2 };

    // JTable table = new JTable(rowData, columnNames);
    JTable table = new JTable(new DefaultTableModel(rowData, columnNames));

    table.setCellSelectionEnabled(true);

    // cell select listener
    ListSelectionModel cellSelectionModel = table.getSelectionModel();
    cellSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    cellSelectionModel.addListSelectionListener(new ListSelectionListener() {
      public void valueChanged(ListSelectionEvent e) {
        String selectedData = null;

        int[] selectedRow = table.getSelectedRows();
        int[] selectedColumns = table.getSelectedColumns();

        for (int i = 0; i < selectedRow.length; i++) {
          for (int j = 0; j < selectedColumns.length; j++) {
            selectedData = (String) table.getValueAt(selectedRow[i], selectedColumns[j]);
          }
        }
        System.out.println("Selected: " + selectedData);
      }

    });










    JButton submitButton = new JButton("Generate DFA");
    submitButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {

            try {

                DefaultTableModel model = (DefaultTableModel) table.getModel();
                int col = table.getColumnCount();
                int row = table.getRowCount();

                System.out.println("~~~~~~~~~~~~");
                for (int i=0; i<row; i++) {
                    for (int j=0; j<col; j++) {
                        System.out.print( model.getValueAt(i, j) );
                        System.out.print("\t");
                    }
                    System.out.println("");
                }

                // loop through row to initialize identifiers
                ArrayList<String> identifiers = new ArrayList<String>();
                // String[] identifiers = new String[];
                for (int i=0; i<row; i++) {
                    identifiers.add( (String) model.getValueAt(i, 0) );
                }
                // System.out.println( identifiers.toString() );

                // if row, col not in identifiers add to identifiers
                // if splitted[index] not in identifiers throw exception
                // col (j) start with 1 (skip identifier col)
                System.out.println("@@@@@@@@@@");
                for (int i=0; i<row; i++) {
                    for (int j=1; j<col; j++) {
                        String identifier = (String) model.getValueAt(i, j);
                        String[] splitted = identifier.split(",");
                        System.out.println( Arrays.toString(splitted) );

                        // if splitted[index] not in identifiers throw exception
                        for (String iden : splitted) {
                            // if iden not in identifiers throw exception
                            if ( !identifiers.contains(iden) && !iden.equals("Ø") ) {
                                System.out.println("Not a valid NFA");
                                // // generate dialog
                                JOptionPane.showMessageDialog(
                                    frame,
                                    String.format("Not a valid NFA for entry %s at row %d col %d!" , iden, i, j)
                                );
                                return;
                            }
                        }


                        // // if identifiers not contain identifier
                        // // add to identifiers
                        // if (!identifiers.contains(identifier) && identifier.toString() != "Ø")
                        //     identifiers.add(identifier);
                    }
                }

                System.out.println("----------");
                System.out.println(identifiers);

                // if rowid not in id throw exception
                // add row
                System.out.println("*************");
                ArrayList<String> deltaCol = Combination.combinations( identifiers.toArray(new String[identifiers.size()]) );
                System.out.println( deltaCol.toString() );
                System.out.println("");

                String[][] dfa = new String[deltaCol.size()][col+1];
                int dfaRow = deltaCol.size();
                int dfaCol = col+1;

                // fill col 1 with alphabets
                int alphabet = 65;
                for (int i=0; i<dfaRow; i++) {
                    dfa[i][0] = String.valueOf( (char) alphabet );
                    alphabet += 1;
                }

                // fill col 2 with deltaCol
                for (int i=0; i<dfaRow; i++) {
                    dfa[i][1] = deltaCol.get(i);
                }

                // fill row 1 with empty list
                for (int i=2; i<dfaCol; i++) {
                    dfa[0][i] = "Ø";
                }





                System.out.println("############");

                for (int i=1; i<dfaRow; i++) {
                    // loop row
                    // if row[ col0 ] contain deltacol(i).split
                    for (int j=2; j<dfaCol; j++) {
                        // split delta[i]
                        // get row deltaN
                        // get index deltaN, table
                        String iden = dfa[i][1];
                        // System.out.println(iden);
                        int[] index = getIndex(iden, table);
                        // System.out.println( Arrays.toString(index) );

                        String temp = new String();
                        for (int r : index) {
                            temp += ( "," + table.getValueAt(r, j-1) );
                        }
                        temp = temp.substring(1);
                        String temp2 = combine(temp);
                        System.out.println(temp2);

                        // push to dfa[i][j]
                        dfa[i][j] = temp2;

                        // ArrayList<String> splitted = (ArrayList<String>) Arrays.asList(deltaCol[i]);
                        // if splitted.contains( table.getValueAt(row, 0) ) {
                        //     // if not empty set
                        //     // push getValueAt(j, k)
                        //     temp =
                        // }
                    }
                }

                // print debug
                for (int i=0; i<dfaRow; i++) {
                    for (int j=0; j<dfaCol; j++) {
                        // System.out.print(dfa[i][j]);
                        System.out.printf( "%1$13s", dfa[i][j] );
                        // System.out.print("\t");

                    }
                    System.out.println("");
                }


                // set frame2 visible
                Object[] dfaColData = new Object[dfaCol];
                dfaColData[0] = "labels";
                for (int i=1; i<dfaCol; i++) {
                    dfaColData[i] = model.getColumnName(i-1);
                }

                JTable table2 = new JTable( new DefaultTableModel(dfa, dfaColData) );
                JScrollPane scrollPane2 = new JScrollPane(table2);


                JFrame dfaFrame = new JFrame("DFA generated");
                dfaFrame.setLayout(new BorderLayout());
                dfaFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

                dfaFrame.add(scrollPane2, BorderLayout.CENTER);
                dfaFrame.setSize(320, 480); //iphone 3g size
                dfaFrame.setVisible(true);
                return;

            } catch (Throwable error) {
                JOptionPane.showMessageDialog(frame, "Not a valid NFA!");
                return;
            }

        }

    });

    JButton addRowButton = new JButton("Add row");
    addRowButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            DefaultTableModel model = (DefaultTableModel) table.getModel();
            model.addRow(new Object[]{"New row"});
        }
    });

    JButton addColButton = new JButton("Add col");
    addColButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            DefaultTableModel model = (DefaultTableModel) table.getModel();
            model.addColumn( Integer.parseInt(table.getColumnName(table.getColumnCount()-1)) + 1 );
        }
    });

    JButton delRowButton = new JButton("Delete row");
    delRowButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            DefaultTableModel model = (DefaultTableModel) table.getModel();
            if (table.getRowCount() > 2)
                model.removeRow( table.getRowCount()-1 );
        }
    });

    JButton delColButton = new JButton("Delete col");
    delColButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            DefaultTableModel model = (DefaultTableModel) table.getModel();
            if (table.getColumnCount() > 3)
                model.setColumnCount( table.getColumnCount()-1 );
        }
    });






    JScrollPane scrollPane = new JScrollPane(table);

    JPanel controlPanel = new JPanel();
    controlPanel.setLayout(new WrapLayout());

    controlPanel.add(addRowButton);
    controlPanel.add(addColButton);
    controlPanel.add(delRowButton);
    controlPanel.add(delColButton);
    controlPanel.add(submitButton);

    frame.add(scrollPane, BorderLayout.CENTER);
    frame.add(controlPanel, BorderLayout.PAGE_END);
    frame.setSize(320, 480); //iphone 3g size
    // frame.setSize(300, 150);
    frame.setVisible(true);

    // System.out.println( Arrays.deepToString(rowData) );
    // System.out.println(rowData[0][0]);
    // System.out.println(columnNames.toString());
    // Object sampleInt = 1;
    // System.out.println(sampleInt.equals( (int) 1 ));

    // TableColumnModel columnModel = table.columnModel;
    // System.out.println(table.columnModel().getColumnCount());

    // // get column count
    // System.out.println(table.getColumnCount());

    // // get last column name
    // System.out.println(table.getColumnName(table.getColumnCount()-1));

    // String string = "z1";
    // String[] splitted = string.split(",");
    // System.out.println(Arrays.toString(splitted));



    // for (int i=0; i<identifiers.length; i++) {
    //     for (int j=i+1; j<identifiers.length; j++) {
    //         System.out.println(identifiers[i] + identifiers[j]);
    //     }
    // }




    // ArrayList<String> outputList = new ArrayList<String>();
    // for (int i=0; i<input.length; i++) {
    //     String[] objectList = Combination.combination(input, i+1);
    //     for (Object obj : objectList) {
    //         outputList.add( (String) obj );
    //     }
    //     // System.out.println(Arrays.toString(objectList));
    // }
    // String[] output = outputList.toArray(new String[outputList.size()]);
    // System.out.println(Arrays.toString(output));




    // for (int i=identifiers.length; i>0; i-- {
    //     identifiers[i] == 0;

    // }

    // int lengthOfList = identifiers.length;
    // for (int i=0; i<(Math.pow(2,lengthOfList)); i++) {
    //     String binaryString = Integer.toBinaryString(i);

    //     for (int j=0; j<lengthOfList; i++) {
    //         if (i%1000)
    //     }

    //     System.out.println(binaryString);

    // }



    String[] input = {"z0", "z1", "z2"};
    // // get unique combination of input string[]
    // String[] output = Combination.combinations(input);
    // System.out.println(Arrays.toString(output));

    // System.out.println( combine("z2,z0", "z1") );

  }
}